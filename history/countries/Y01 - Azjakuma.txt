
setup_vision = yes
government = theocracy
add_government_reform = tagharoghi_reform
government_rank = 2
primary_culture = horned_ogre
add_accepted_culture = hill_yan
religion = lefthand_path
technology_group = tech_eastern_ogre
capital = 5430
historical_rival = Y03 # Hulibao
historical_friend = Y89 # Yuantsai

1000.1.1 = { set_estate_privilege = estate_mages_organization_religious }

1425.3.10 = {
	monarch = {
		name = "Tiraga"
		dynasty = "Aromo"
		adm = 5
		dip = 3
		mil = 3
		birth_date = 1372.3.10
	}
	add_ruler_personality = careful_personality
	add_ruler_personality = calm_personality
	heir = {
		name = "Akira"
		dynasty = "Surgoshi"
		birth_date = 1415.3.10
		death_date = 1481.3.2
		claim = 95
		adm = 4
		dip = 3
		mil = 6
	}
	add_heir_personality = conqueror_personality
}