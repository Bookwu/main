
country_decisions = {

	# Unite one_xia - war
	form_one_xia = {
		major = yes
		potential = {
			has_reform = wulin
			NOT = { tag = Y99 }
			NOT = { has_country_flag = formed_one_xia_flag }
			was_never_end_game_tag_trigger = yes
			any_country = {
				vassal_of = ROOT
				has_reform = xiaken
			}
		}
		provinces_to_highlight = {
			OR = {
				province_id = 4559
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			NOT = { exists = Y99 }
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 30
			owns = 4559
		}
		effect = {
			remove_government_reform = wulin
			add_government_reform = one_xia
			set_government_rank = 3
			custom_tooltip = unite_one_xia_subject_tooltip
			hidden_effect = {
				if = {
					limit = { has_reform = wulin }
					add_devotion = 100
				}
				every_subject_country = {
					limit = {
						has_reform = xiaken
					}
					ROOT = {
						free_vassal = PREV
					}
					remove_government_reform = xiaken
					#set_legacy_government = despotic_monarchy
					every_owned_province = {
						remove_core = Y99
					}
				}
			}
			change_tag = Y99
			add_devotion = 50
			hidden_effect = { restore_country_name = yes }
			set_country_flag = formed_one_xia_flag
			swap_non_generic_missions = yes
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { num_of_cities = 30 }
			}
		}
	}
	
	# United one_xia - peace
	united_one_xia = {
		major = yes
		potential = {
			has_reform = wulin
			NOT = { has_country_flag = formed_one_xia_flag }
			was_never_end_game_tag_trigger = yes
			NOT = { exists = Y99 }
		}
		allow = {
			NOT = { exists = Y99 }
			is_free_or_tributary_trigger = yes
			owns = 4559
			all_country = {
				NOT = {
					has_reform = xiaken
				}
				NOT = {
					has_reform = indep_xiaken
				}
			}
			is_at_war = no
		}
		effect = {
			change_tag = Y99
			hidden_effect = { restore_country_name = yes }
			remove_government_reform = wulin
			add_government_reform = one_xia
			set_country_flag = formed_one_xia_flag
			set_government_rank = 3
			add_legitimacy = 100
			add_adm_power = 100
			add_dip_power = 100
			add_mil_power = 100
			add_devotion = 50
			swap_non_generic_missions = yes
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			hidden_effect = {
				if = {
					limit = { has_reform = wulin }
					add_devotion = 100
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	righteous_conclave = {
		major = yes
		potential = {
			has_reform = one_xia
			NOT = {
				has_reform = xia_parliament_reform
				has_country_modifier = xia_summit_ended
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			custom_trigger_tooltip = {
				tooltip = xia_righteous_conclave_allow_tooltip
				OR = {
					has_reform = partial_secularisation_reform
					has_reform = maintain_religious_head_reform
					has_reform = hereditary_religious_leadership_reform
					has_reform = crown_leader_reform
					has_reform = proclaim_republic_reform
					has_reform = battle_pope_reform
					has_reform = conciliarism_reform
				}
			}
		}
		effect = {
			country_event = { id = xia_summit.2 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	claim_kongjian = {
		major = yes
		potential = {
			OR = {
				has_reform = wulin
				has_reform = xiaken
				has_reform = indep_xiaken
				has_reform = one_xia
			}
			mission_completed = xia_a_balanced_soul
			owns_core_province = 4811
		}
		allow = {
			NOT = {
				has_country_modifier = xia_wielding_kongjian
				has_ruler_modifier = xia_failed_trial_of_balance
				ruler_has_personality = mage_personality
				has_country_flag = xia_in_trial_of_balance
			}
		}
		effect = {
			if = {
				limit = { NOT = { has_country_modifier = xia_ended_the_keepers } }
				country_event = { id = one_xia.70 }
				hidden_effect = {
					xia_determine_affinity = yes
					set_country_flag = xia_in_trial_of_balance
				}
			}
			else_if = {
				limit = { has_country_modifier = xia_ended_keepers }
				add_ruler_modifier = {
					name = xia_wielding_kongjian
					duration = -1
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}
