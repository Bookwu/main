
defined_text = {
	name = GetMagesOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_mages_name
		trigger = {
			has_estate = estate_mages
		}
	}
	
	text = {
		localisation_key = estate_mages
		trigger = {
			NOT = { has_estate = estate_mages }
		}
	}
}

defined_text = {
	name = GetArtificersOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_artificers_name
		trigger = {
			has_estate = estate_artificers
		}
	}
	
	text = {
		localisation_key = estate_artificers
		trigger = {
			NOT = { has_estate = estate_artificers }
		}
	}
}

defined_text = {
	name = GetAdventurersOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_adventurers_name
		trigger = {
			has_estate = estate_adventurers
		}
	}
	
	text = {
		localisation_key = estate_adventurers
		trigger = {
			NOT = { has_estate = estate_adventurers }
		}
	}
}

defined_text = {
	name = GetMonstrousTribesOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_monstrous_tribes_name
		trigger = {
			has_estate = estate_monstrous_tribes
		}
	}
	
	text = {
		localisation_key = estate_monstrous_tribes
		trigger = {
			NOT = { has_estate = estate_monstrous_tribes }
		}
	}
}

defined_text = {
	name = GetRajMinistriesOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_raj_ministries_name
		trigger = {
			has_estate = estate_raj_ministries
		}
	}
	
	text = {
		localisation_key = estate_raj_ministries
		trigger = {
			NOT = { has_estate = estate_raj_ministries }
		}
	}
}

defined_text = {
	name = GetHorseRaceFirst
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceSecond
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceThird
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceFourth
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceFifth
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceSixth
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceColour
	random = no
	
	text = {
		localisation_key = horseRaceOptionBlue
		trigger = {
			province_id = event_target:blue_origin
		}
	}
	text = {
		localisation_key = horseRaceOptionTeal
		trigger = {
			province_id = event_target:teal_origin
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			province_id = event_target:green_origin
		}
	}
	text = {
		localisation_key = horseRaceOptionYellow
		trigger = {
			province_id = event_target:yellow_origin
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			province_id = event_target:orange_origin
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			province_id = event_target:red_origin
		}
	}
}