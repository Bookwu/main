
##BranchEnd 0
##BranchStart 1
yingshi_start_1 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = Y10
	}
	has_country_shield = yes
	
	##Mission Start
	yingshi_a_keeper_of_secrets = {
		icon = mission_dark_book
		required_missions {
			yingshi_lies_and_slander
		}
		position = 2

		
		trigger = {
			spymaster = 1
		
		}
		effect = {
			add_government_reform = sinistral_chamber
			ROOT = {
				country_event = { 
				id = yingshi.4
				}
			}
		}
	}
	##Mission End
	##Mission Start
	yingshi_oni_overture = {
		icon = mission_oni
		required_missions {
			yingshi_a_keeper_of_secrets
		}
		position = 3

		trigger = {
			OR = {
				Y01 = {
					AND = {
					has_opinion = {
						who = ROOT	
						value = 0
						}
					is_at_war = no
					}
				}
				NOT = { exists = Y01 }
				}
		}
		effect = {
			if {
				limit = {
					exists = Y01
					}
					
				Y01 = {
					country_event = {
						id = yingshi.5
						}
					}
				}
				
			else {
				add_country_modifier = {
					name = yingshi_oni_knowledge
					duration = 9125
				}
			}
		}
	}
	##Mission End
	##Mission Start
	yingshi_fulfill_the_contract = {
		icon = mission_assassination
		required_missions {
			yingshi_oni_overture
		}
		position = 4
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
		
			OR = {
			any_country = {
				AND = {
					is_rival = Y01
					has_spy_network_from = {
						who = ROOT
						value = 60
						}
					}
				}
			NOT = { exists = Y01 }
				
			}	
		}
		effect = {
			if = {
				limit = {
					exists = Y01
					}
			ROOT = {
				country_event = {
					id = yingshi.7
						}
					}
		hidden_effect = {
			random_country = {
				limit = {
					AND = {
						is_rival = Y01
						has_spy_network_from = {
							who = ROOT
							value = 60
						}
					}
				}
			
			country_event = {
				id = yingshi.8
					}	
				}
			}
		}
	}
}
	##Mission End
	##Mission Start
	ying_best_things = {
		icon = mission_crowning
		required_missions {
			yingshi_fulfill_the_contract yingshi_corrupt_the_patrol yingshi_a_shield_against_zealots
		}
		position = 5
		provinces_to_highlight = {

				
		}
		trigger = {
			any_rival_country = {
				has_spy_network_from = {
					who = ROOT
					value = 100 }
							}
						
		}
		effect = {
			hidden_effect = {
						random_rival_country = {
							limit = {
								has_spy_network_from = {
								who = ROOT
								value = 100 
								}
							}
						}			
					country_event = {
						id = yingshi.11
						days = 3
					}	
				}
			
						
		
			country_event = {
				id = yingshi.10
				}
			custom_tooltip = y10_assassin_events_tooltip
			hidden_effect = {
				set_country_flag = allow_yingshi_events
			}
		}
	}
	##Mission End
	##
	
}
##BranchEnd 1
##BranchStart 2
yingshi_start_2 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = Y10
	}
	has_country_shield = yes
	
	##Mission Start
	yingshi_lies_and_slander = {
		icon = mission_missive
		required_missions {
			
		}
		position = 1

		trigger = {
			OR = {
				AND = {
			
				has_spy_network_in = {
					who = Y02
					value = 40
					}
				Y01 = {
					exists = yes
					}
		}
				
				AND = {
					Y02 = {
						exists = no
					}
					owns_core_province = 4851
		}
	}
}
		effect = {
			if {
				limit = {
					AND = {
					
					Y02 = {
						exists = yes
					}
					
					Y01 = {
						exists = yes
					}
				}
			}
				ROOT = {
					country_event = {
						id = yingshi.1
					}
				}

			}
			zujiang_area = { add_permanent_claim = ROOT }
			4952 = { add_permanent_claim = ROOT }
			
			add_spy_network_in = {
					who = Y02
					value = -40
				}
			
			
		}
	}

	##Mission End
	##Mission Start
	yingshi_never_saw_it_coming = {
		icon = mission_whisper
		required_missions {
			yingshi_lies_and_slander
		}
		position = 2
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			OR = {
				AND = {
			
				has_spy_network_in = {
					who = Y04
					value = 50
					}
				church_power = 75
		}
				
				AND = {
					Y04 = {
						exists = no
					}
					owns_core_province = 4853
					owns_core_province = 4933
					owns_core_province = 4854
			}
		}
	}
		
		effect = {
				ROOT = {
					country_event = {
						id = yingshi.9
					}
				}
				4933 = { add_permanent_claim = ROOT }
				4853 = { add_permanent_claim = ROOT }
				4854 = { add_permanent_claim = ROOT }
				
				add_spy_network_in = {
					who = Y04
					value = -50
				}
		}
}
	##Mission End
	##Mission Start
	yingshi_steal_the_vote = {
		icon = mission_writing_book
		required_missions {
			yingshi_never_saw_it_coming yingshi_a_keeper_of_secrets
		}
		position = 3
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
		
			OR = {
				AND = {
				has_spy_network_in = {
					who = Y06
					value = 60
					}
				treasury = 150
					}
				
				owns_core_province = 4861
				}
		}
		effect = {
			country_event = { 
				id = yingshi.15	
						}
			add_treasury = -150
						
			add_spy_network_in = {
					who = Y06
					value = -60
				}			
			
			hidden_effect = {
				country_event = {
					id = yingshi.18
					days = 360
						}
			}
		}
	}
	##Mission End
	##Mission Start
	yingshi_corrupt_the_patrol = {
		icon = mission_city_prosperity
		required_missions {
			yingshi_steal_the_vote
		}
		position = 4
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
				has_spy_network_in = {
					who = Y08
					value = 70
					}
				manpower = 5
		}
		effect = {
			country_event = { 
				id = yingshi.19
						}
			add_manpower = -5
			
			add_spy_network_in = {
					who = Y08
					value = -50
				}
		}
	}
	##Mission End
	##
	
}
##BranchEnd 2
##BranchStart 3
yingshi_start_3 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = Y10
	}
	has_country_shield = yes
	

	##Mission Start
	yingshi_taking_it_back = {
		icon = mission_asian_city
		required_missions {
			yingshi_lies_and_slander
		}
		position = 2
		provinces_to_highlight = {
			OR = {
				province_id = 4952
				area = zujiang_area
			}
			NOT = {
				owned_by = ROOT
			}
		}
		trigger = {
			AND = {
				4952 = { owned_by = ROOT }
				zujiang_area = { 
					type = all
					owned_by = ROOT 
				}
			}
		}
		effect = {
			bianfang_area = { add_permanent_claim = ROOT }
			biantian_area = { add_permanent_claim = ROOT }
		}
	}
	##Mission End
	##Mission Start
	yingshi_break_the_fang = {
		icon = mission_destroyed_city
		required_missions {
			yingshi_taking_it_back
		}
		position = 3
		provinces_to_highlight = {
			OR = {
				area = biantian_area
				area = bianfang_area
			}
			NOT = {
				owned_by = ROOT
			}
		}

		trigger = {
			AND = {
				biantian_area = { 
					type = all
					owned_by = ROOT 
				}
				bianfang_area = { 
					type = all
					owned_by = ROOT 
				}
		}
	}
		effect = {
			4826 = {
				add_base_tax = -3
				add_base_production = -3
				add_base_manpower = -3
				}
			4842 = {
				add_base_tax = -3
				add_base_production = -3
				add_base_manpower = -3
				}
			4849 = {
				add_base_tax = 4
				add_base_production = 4
				add_base_manpower = 4
				}
		}
	}
	##Mission End
	##Mission Start
	yingshi_a_shield_against_zealots = {
		icon = mission_burning_parthenon
		required_missions {
			yingshi_break_the_fang
		}
		position = 4
		provinces_to_highlight = {

		}
		trigger = {
			4826 = { 
				owned_by = ROOT
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
					}
				has_building = barracks
				}
			
		}
		effect = {
			4826 = {
				add_building_construction = {
					building = ramparts
					speed = 1
					cost = 0.5
				}
			}
			
			add_country_modifier = {
				name = yingshi_shield
				duration = 9100
			}
		}
	}
	##Mission End
	##
	
}
##BranchEnd 3
