###########################################
#		Start>Unite the townships stuff		#
###########################################


hiderion_branch_azkare = { #Hiderion improvement branch
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { tag = Y86 }
	
	azkare_gather_advisors = {
		icon = mission_discussion_arch
		position = 1
		required_missions = { }
		
		trigger = {
			OR = {
				employed_advisor = {
					category = ADM
					culture = sikai
				}
				employed_advisor = {
					category = DIP
					culture = sikai
				}
				employed_advisor = {
					category = MIL
					culture = sikai
				}
			}
		}
		
		effect = {
			country_event = { id = azkare.5 } #Sons of Azkare
		}
	}
	
	azkare_tour_the_towns = {
		icon = mission_armor_choice
		position = 2
		required_missions = {
			azkare_gather_advisors
		}
		
		trigger = {
			is_at_war = no
			adm_power = 50
			dip_power = 50
		}
		
		effect = {
			add_adm_power = -50
			add_dip_power = -50
			country_event = { id = azkare.6 } #Tour the New Towns
		}
	}
	
	azkare_hiderions_contemplation = {
		icon = mission_women_prayer
		position = 3
		required_missions = {
			azkare_tour_the_towns
		}
		
		trigger = {
			is_at_war = no
			adm_power = 100
			dip_power = 100
			mil_power = 100
			treasury = 50
		}
		
		effect = {
		add_country_modifier = {
			name = Y86_hiderions_contemplation_modifier
			duration = 36 #3 Years
		}
		country_event = { id = azkare.7 } #Hiderion's Contemplation
		}
	}
}

khao_elnak_branch_azkare = { #Khao Elnak Conquest Branch/Lady Mya Satae
	slot = 2
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { tag = Y86 }
	
	azkare_integrate_new_towns = {
		icon = mission_early_game_buildings
		position = 1
		required_missions = {
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4790
				province_id = 4787
				province_id = 5425
			}
		}
		
		trigger = {
			adm_power = 50
			treasury = 50
		}
		
		effect = {
			country_event = { id = azkare.8 } #New Recruits
		}
	}

	azkare_conquer_khao_elnak = {
		icon = mission_green_village
		position = 2
		required_missions = {
		azkare_integrate_new_towns
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4788 
				province_id = 4792
				province_id = 4791
				}
		}
		
		trigger = {
			 4792 = {
			 owned_by = ROOT
			 }
			 4791 = {
			 owned_by = ROOT
			 }
			 4788 = {
			 owned_by = ROOT
			 }
		}
		
		effect = {
			4947 = {
			add_permanent_claim = ROOT
			}
			dekphre_area = {
			add_permanent_claim = ROOT
			}
			sadramver_area = {
			add_permanent_claim = ROOT
			}
		}
	}
	
	azkare_the_lady = {
		icon = mission_female_throne
		position = 2
		required_missions = {
		azkare_conquer_khao_elnak
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4792 
				province_id = 4788
				province_id = 4791
			}
		}
		
		trigger = {
			NOT = {
				exists = Y62 #Khao Elnak
			}
			owns_core_province = 4792
			owns_core_province = 4788
			owns_core_province = 4791
			dip_power = 50
		}
		
		effect = {
			add_dip_power = -50
			country_event = { id = azkare.9 } #The Lady Mya Saetae
		}
	}
}

ngoen_branch_azkare = { #Lo Ngoen conquest/ The Lord Rama Buakae/Guardian of the Townships
	slot = 3
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { tag = Y86 }
	
	azkare_towards_the_future = {
		icon = mission_manchu_warrior
		position = 1
		required_missions = {
		}
		
		provinces_to_highlight = {
		province_id = 4789
		}
		
		trigger = {
			army_size_percentage = 0.8
			4789 = {
			cavalry_in_province = 2
			}
		}
		
		effect = {
			ngoen_area = {
			add_permanent_claim = ROOT
			}
		}
	}
	
	azkare_finish_lo_ngoen = {
		icon = mission_conquer_5_states
		position = 2
		required_missions = {
		azkare_towards_the_future
		}
		
		provinces_to_highlight = {
			area = ngoen_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			ngoen_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_prestige = 10
		}
	}
	
	azkare_the_lord = {
		icon = mission_thinking_king
		position = 3
		required_missions = {
		azkare_finish_lo_ngoen
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4940
				province_id = 4777
				province_id = 4781
			}
		}
		
		trigger = {
			NOT = {
			exists = Y60 #Lo Ngoen
			}
			owns_core_province = 4777
			owns_core_province = 4940
			owns_core_province = 4781
			mil_power = 50
		}
		
		effect = {
			add_mil_power = -50
			country_event = { id = azkare.10 } #The Lord Rama Buakae
		}
	}
	
	azkare_guardian_of_the_townships = {
		icon = mission_big_house
		position = 4
		required_missions = {
		azkare_hiderions_contemplation azkare_the_lord azkare_the_lady azkare_the_brothers azkare_a_flourishing_economy
		}
		
		provinces_to_highlight = { 
			OR = {
			area = ngoen_area
			area = kaiden_area
			area = phakphon_area
			area = thirabnir_area
			}
		}
		
		trigger = {
			ngoen_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			kaiden_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			phakphon_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			thirabnir_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier =  {
				name = Y86_guardian_of_the_townships
				duration = 3650
				}
			country_event = { id = azkare.11 } #Further Ambitions
			add_government_reform = azkare_court # Needs to be added
		}
	}
}
	
thirabnir_branch_azkare = { #Thirabnir vassalization/The Brothers
	slot = 4
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { tag = Y86 }
	
	azkare_visiting_an_old_friend = {
		icon = mission_writing_book
		position = 1
		required_missions = {
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4778
				province_id = 4789
			}
		}
		
		trigger = {
			OR = {
				Y59 = {
					has_opinion = {
						who = ROOT
						value = 150
					}
					alliance_with = Y86 #Azkare
				}
				NOT = {
					exists = Y59
				}	
			}
			dip_power = 100
		}
		
		effect = {
			add_dip_power = -100
			Y59 = {
				country_event = { id = azkare.12 }
			}
		}
	}
	
	azkare_a_shared_dream = {
		icon = mission_four_men
		position = 2
		required_missions = {
		azkare_visiting_an_old_friend
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4778
				province_id = 4789
			}
		}
		
		trigger = {
			OR = {
				Y59 = {
					is_subject_of = Y86 #Azkare
					has_opinion = {
						who = ROOT
						value = 200
					}
					NOT = {
						liberty_desire = 1
					}
				}
				NOT = {
					exists = Y59 #Thirabnir
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = Y86_a_dream_shared 
				duration = 7200
				}
			country_event = { id = azkare.13 } #A Dream Shared
		}
	}
	
	azkare_the_brothers = {
		icon = mission_confrontation
		position = 3
		required_missions = {
		azkare_a_shared_dream
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4943
				province_id = 4780
				province_id = 4778
				province_id = 4779
			}
		}
		
		trigger = {
			NOT = {
				exists = Y59 #Thirabnir
			}
			thirabnir_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			adm_power = 50
		}
		
		effect = {
			add_adm_power = -50
			country_event = { id = azkare.14 } #The Brothers Chaiya
		}
	}
}

economy_branch_azkare = { #Develop Azkare/townships
	slot = 5
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { tag = Y86 }
	
	azkare_khanvuphaim_industry = {
		icon = mission_se_birdmen
		position = 1
		required_missions = {
		}
		
		provinces_to_highlight = {
		 province_id = 4789
		}
		
		trigger = {
			4789 = {
				has_production_building_trigger = yes
				base_production = 8
			}
		}
		
		effect = {
			4789 = {
				add_province_modifier = {
					name = Y86_khanvuphaim_industry
					duration = -1
				}
			}
		}
	}
	
	azkare_sikai_purebreds = {
		icon = mission_cossack_cavalry
		position = 2
		required_missions = {
		azkare_khanvuphaim_industry
		}
		
		provinces_to_highlight = {
			OR = {
			province_id = 4790
			province_id = 4791
			province_id = 4792
			}
		}
		
		trigger = {
			owns_core_province = 4790
			4790 = {
				base_production = 4
				base_manpower = 3
				has_manufactory_trigger = yes
			}
			owns_core_province = 4791
			4791 = {
				base_production = 4
				base_manpower = 3
				has_manufactory_trigger = yes
			}
			owns_core_province = 4792
			4792 = {
				base_production = 4
				base_manpower = 3
				has_manufactory_trigger = yes
			}
		}
		
		effect = {
			add_country_modifier = {
				name = Y86_sikai_purebred
				duration = 7200
			}
		}
	}
	
	azkare_a_flourishing_economy = {
		icon = mission_city_prosperity
		position = 3
		required_missions = {
		azkare_sikai_purebreds
		}
		
		provinces_to_highlight = {
			OR = {
				area = thirabnir_area
				area = kaiden_area
				area = ngoen_area
				area = phakphon_area
			}
		}
		
		trigger = {
			monthly_income = 15
		}
		
		effect = {
			add_country_modifier = {
				name = Y86_growing_economy
				duration = 7200
			}
		}
	}
}

###########################################
#				Elf related stuff				#
###########################################
citadel_azkare = { #Improve the Citadel
	slot = 4
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { 
		tag = Y86 
		has_country_flag = azkare_united_townships #Gets revealed after Guardian of the Townships Mission
		}
	
	azkare_improve_the_citadel = {
		icon = mission_japanese_fort
		position = 4
		required_missions = {
		azkare_a_flourishing_economy
		}
		
		provinces_to_highlight = {
		 province_id = 4789 #Azkare
		}
		
		trigger = { #REMEMBER TO ADD A CUSTOM TOOLTIP WHEN YOU KNOW HOW TO DO THAT FOR THE ELF
			4789 = { #Azkare
				base_manpower = 6
				has_manpower_building_trigger = yes
				has_fort_building_trigger = yes
			}
			employed_advisor = {
				type = fortification_expert
				}
		}
		
		effect = {
			if = {  #If elf advisor, also gain 2 military development, otherwise just gain improved modifier
				limit = { 
					employed_advisor = {
						culture = sunrise_elf
						type = fortification_expert
					}
				}
					4789 = { #Azkare
						remove_province_modifier = Y86_citadel_of_dawn_azkare
						add_province_modifier = {
							name = Y86_citadel_of_dawn_azkare_2
							duration = -1
						}
						add_base_manpower = 2
					}
			}
			else = {
				4789 = { #Azkare
					remove_province_modifier = Y86_citadel_of_dawn_azkare
					add_province_modifier = {
						name = Y86_citadel_of_dawn_azkare_2
						duration = -1
					}
				}
			}
		}
	}
}	

restore_reputation_azkare = { #Sending the Call, improve elf reputation
	slot = 5
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { 
		tag = Y86 
		has_country_flag = azkare_united_townships
		}
	
	
	
	azkare_jaerels_values = {
		icon = mission_tree_fishing
		position = 5
		required_missions = {
		azkare_restore_our_reputation azkare_improve_the_citadel
		}
		
		provinces_to_highlight = {
			OR = {
				area = thirabnir_area
				area = kaiden_area
				area = phakphon_area
				area = ngoen_area
			}
		}
		
		trigger = {	
			has_institution = renaissance
			estate_loyalty = { estate = estate_nobles loyalty = 65 } #Add custom tooltip for the estates so it's one line
			estate_loyalty = { estate = estate_burghers loyalty = 65 }
			estate_loyalty = { estate = estate_church loyalty = 65 }
			estate_loyalty = { estate = estate_adventurers loyalty = 65 }
			if = {
				limit = { has_estate = estate_mages }
				estate_loyalty = { estate = estate_mages loyalty = 50 }
			}
			custom_trigger_tooltip = {
				tooltip = azkare_jaerels_values_prosperous_tooltip
				thirabnir_area = {
					is_prosperous = yes
				}
				kaiden_area = {
					is_prosperous = yes
				}
				phakphon_area = {
					is_prosperous = yes
				}
				ngoen_area = {
					is_prosperous = yes
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = Y86_jaerels_values
				duration = 7200 #20 Years
			}
		}
	}
	
	azkare_sending_the_call = {
		icon = mission_diplomacy_missive
		position = 6
		required_missions = {
		azkare_jaerels_values
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			diplomatic_reputation = 2
			monthly_adm = 10
			monthly_dip = 10
			monthly_mil = 10
			custom_trigger_tooltip = {
				tooltip = azkare_sending_the_call_tooltip
				OR = {
					employed_advisor = {
						culture = sunrise_elf
						type = diplomat
					}
				}
				OR = {
					employed_advisor = {
						culture = sunrise_elf
						type = trader
					}	
				}
			}
		}
		
		effect = {
			country_event = { id = azkare.15 } #Gathering the Lost
			set_country_flag = azkare_sent_the_call
		}
	}
	
	azkare_bowmasters_of_lektonmai = {
		icon = mission_non-western_cavalry_raid
		position = 7
		required_missions = {
		azkare_sending_the_call
		}
		
		provinces_to_highlight = {
			province_id = 4787
		}
		
		trigger = {
			4787 = {
				base_manpower = 6
				has_manpower_building_trigger = yes
			}
		}
		
		effect = {
			add_country_modifier = {
				name = Y86_elven_bowmasters
				duration = 7200 #20 Years
				}
		}
	}
}

###########################################
#		Azkare>Sunrise Empire stuff			#
###########################################

bomdan_azkare = { #Sending the Call, improve elf reputation
	slot = 1
	generic = no
	ai = yes 
	has_country_shield = yes
	potential = { 
		tag = Y86 
		has_country_flag = azkare_united_townships
		}
	
	azkare_contact_lesser_daulophs = {
		icon = mission_princess_writing
		position = 5
		required_missions = {
		azkare_guardian_of_the_townships
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	azkare_conquer_lapnam_amrik = {
		icon = mission_tree_fishing
		position = 6
		required_missions = {
		azkare_contact_lesser_daulophs
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	azkare_secure_the_telebei = {
		icon = mission_diplomacy_missive
		position = 7
		required_missions = {
		azkare_conquer_lapnam_amrik
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	azkare_steward_of_the_necropolis = {
		icon = mission_non-western_cavalry_raid
		position = 8
		required_missions = {
		azkare_secure_the_telebei
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	azkare_hermits_and_hunter = {
		icon = mission_non-western_cavalry_raid
		position = 9
		required_missions = {
		azkare_steward_of_the_necropolis
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	azkare_new_dawn_porcelain = {
		icon = mission_non-western_cavalry_raid
		position = 10
		required_missions = {
		azkare_steward_of_the_necropolis
		}
		
		provinces_to_highlight = {
		 
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
}
	