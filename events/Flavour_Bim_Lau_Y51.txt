namespace = bim_lau_flavour

country_event = { #Restore the necropolis?
	id =  bim_lau_flavour.1
	title = bim_lau_flavour.1.t
	desc = bim_lau_flavour.1.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		always = yes
	}
	
	option = { #restore
		name = bim_lau_flavour.1.a
		custom_tooltip = bim_lau_restore_necropolis_tt
		
		add_estate_loyalty = {
			estate = estate_church
			loyalty = -10
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombOpulence
					value = 10
				}
			}
			set_country_flag = restore_bim_lau_necropolis
		}
		
		ai_chance = {
			factor = 90
		}
	}
	
	option = { #exploit
		name = bim_lau_flavour.1.b
		custom_tooltip = bim_lau_restore_necropolis_tt
		
		add_estate_loyalty = {
			estate = estate_church
			loyalty = 10
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -10
		}
		
		hidden_effect = {
			set_country_flag = exploit_bim_lau_necropolis
		}
		
		ai_chance = {
			factor = 10
		}
	}
}

country_event = { #revolt for the necropolis
	id = bim_lau_flavour.2
	title = bim_lau_flavour.2.t
	desc = bim_lau_flavour.2.d
	picture = NOBLE_ESTATE_DEMANDS_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		always = yes
	}
	
	immediate = {
		hidden_effect = {
			add_legitimacy = -25
		}
	}
	
	option = { #make concessions
		name = bim_lau_flavour.2.a
		
		add_years_of_income = -3
		every_owned_province = {
			add_local_autonomy = 35
		}
		
		4565 = {
			remove_province_modifier = Y51_necropolis_derelict
			add_permanent_province_modifier = {
				name = Y51_necropolis_repairs
				duration = -1
			}
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombOpulence
					value = 5
				}
			}
		}
		
		ai_chance = {
			factor = 10
		}
	}
	
	option = { #royal family is killed
		name = bim_lau_flavour.2.b
		
		kill_ruler = yes
		kill_heir = yes
		
		define_ruler = {
			culture = ranilau
			age = 53
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
			hide_skills = yes
			claim = 30
		}
		
		4565 = {
			remove_province_modifier = Y51_necropolis_derelict
			add_permanent_province_modifier = {
				name = Y51_necropolis_repairs
				duration = -1
			}
		}
		
		ai_chance = {
			factor = 1
		}
	}
	
	option = { #estates pay for restoration
		name = bim_lau_flavour.2.c
		highlight = yes
		
		trigger = {
			estate_loyalty = {
				estate = estate_church
				loyalty = 70
			}
			estate_influence = {
				estate = estate_church
				influence = 50
			}
			estate_loyalty = {
				estate = estate_nobles
				loyalty = 70
			}
			estate_influence = {
				estate = estate_nobles
				influence = 50
			}
		}
		
		random_owned_province = {
			limit = { NOT = { has_province_flag = bim_lau_autonomy } }
			add_local_autonomy = 15
			hidden_effect = { set_province_flag = bim_lau_autonomy }
		}
		random_owned_province = {
			limit = { NOT = { has_province_flag = bim_lau_autonomy } }
			add_local_autonomy = 15
			hidden_effect = { set_province_flag = bim_lau_autonomy }
		}
		random_owned_province = {
			limit = { NOT = { has_province_flag = bim_lau_autonomy } }
			add_local_autonomy = 15
			hidden_effect = { set_province_flag = bim_lau_autonomy }
		}
		hidden_effect = {
			every_owned_province = {
				limit = { has_province_flag = bim_lau_autonomy }
				clr_province_flag = bim_lau_autonomy
			}
		}
		
		4565 = {
			remove_province_modifier = Y51_necropolis_derelict
			add_permanent_province_modifier = {
				name = Y51_necropolis_repairs
				duration = -1
			}
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombOpulence
					value = 10
				}
			}
		}
		
		ai_chance = {
			factor = 1000
		}
	}
}

country_event = { #the succession
	id = bim_lau_flavour.3
	title = bim_lau_flavour.3.t 
	desc = bim_lau_flavour.3.d 
	picture = BAD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		always = yes
	}
	
	option = { #harimari eldest son
		name = bim_lau_flavour.3.a 
		
		trigger = {
			ruler_culture = royal_harimari
		}
		
		ai_chance = {
			factor = 75
		}
		
		define_heir = {
			culture = royal_harimari
			dynasty = ROOT
			age = 23
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
			hide_skills = yes
		}
		hidden_effect = {
			set_country_flag = bim_lau_normal_missions
		}
		swap_non_generic_missions = yes
	}
	
	option = { #harimari supremacy
		name = bim_lau_flavour.3.b 
		
		trigger = {
			ruler_culture = royal_harimari
		}
		
		ai_chance = {
			factor = 25
		}
		
		define_heir = {
			culture = royal_harimari
			dynasty = ROOT
			age = 16
			adm = 0
			dip = 0
			mil = 6
			max_random_adm = 6
			max_random_dip = 6
			hide_skills = yes
		}
		
		change_primary_culture = royal_harimari
		hidden_effect = {
			set_country_flag = harimari_supremacy_succession
		}
		swap_non_generic_missions = yes
		
		add_country_modifier = {
			name = "army_reform"
			duration = 9125
		}
	}
	
	option = { #human option Ang Rang dynasty returns
		name = bim_lau_flavour.3.c 
		
		trigger = {
			primary_culture = ranilau
			ruler_culture = ranilau
		}
		
		ai_chance = {
			factor = 125
		}
	
		define_heir = {
			culture = ranilau
			dynasty = "Ang Rang"
			age = 14
			adm = 4
			dip = 5
			mil = 4
			hide_skills = yes
			claim = 30
		}
		hidden_effect = {
			set_country_flag = bim_lau_normal_missions
		}
		set_country_flag = ang_rang_dynasty_returned
		swap_non_generic_missions = yes
	}
	
	option = { #human option refuse Ang Rang dynasty
		name = bim_lau_flavour.3.e
		
		trigger = {
			primary_culture = ranilau
			ruler_culture = ranilau
		}
		
		ai_chance = {
			factor = 25
		}
		
		add_legitimacy = -20
		
		every_owned_province = {
			limit = {
				culture = ranilau
			}
			add_unrest = 5
		}
		
		define_heir = {
			culture = ranilau
			dynasty = ROOT
			age = 14
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 5
			max_random_dip = 5
			max_random_mil = 5
			hide_skills = yes
			claim = 60
		}
		hidden_effect = {
			set_country_flag = bim_lau_normal_missions
		}
		swap_non_generic_missions = yes
	}
	
	option = { #in case something unexpected happened
		name = bim_lau_flavour.3.a
		
		trigger = {
			NOT = { ruler_culture = ranilau }
			NOT = { ruler_culture = royal_harimari }
		}
		
		define_heir = {
			dynasty = ROOT
			age = 14
			adm = 0
			dip = 0
			mil = 0
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
		}
		hidden_effect = {
			set_country_flag = bim_lau_normal_missions
		}
		swap_non_generic_missions = yes
	}
}

#Necropolis Setup event
country_event = {
	id = bim_lau_flavour.4
	title = bim_lau_flavour.4.t 
	desc = bim_lau_flavour.4.d 
	picture = DIPLOMACY_eventPicture
	
	hidden = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		always = yes
	}
	
	immediate = {
		4565 = {
			set_variable = {
				which = BimLauTombPower
				value = 0
			}
			set_variable = {
				which = BimLauTombOpulence
				value = 0
			}
		}
		bim_lau_spirit_power = yes
	}
	
	option = {
		name = bim_lau_flavour.4.a
	}
}

#Great Necropolis Entombment
country_event = {
	id = bim_lau_flavour.5
	title = bim_lau_flavour.5.t
	desc = bim_lau_flavour.5.d
	picture = DEATH_OF_HEIR_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	#Large Ceremony
	option = {
		name = bim_lau_flavour.5.a
		custom_tooltip = bim_lau_spirit_update_tt
		ai_chance = {
			factor = 15
		}
		add_years_of_income = -2
		add_prestige = 10
		4565 = {
			random_list = {
				33 = {
					add_base_tax = 1
				}
				33 = {
					add_base_production = 1
				}
				33 = {
					add_base_manpower = 1
				}
			}
		}
		every_province = {
			limit = {
				owned_by = ROOT
				region = bomdan_region
			}
			add_province_modifier = {
				name = Y58_lot_dekkhang_ceremony
				duration = 1825
			}
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombOpulence
					value = 5
				}
			}
			if = {
				limit = {
					has_country_flag = bim_lau_mage_entombment
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 5
					}
				}
				clr_country_flag = bim_lau_mage_entombment
			}
			else_if = {
				limit = {
					has_country_flag = bim_lau_exceptional_spirit
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 3
					}
				}
				clr_country_flag = bim_lau_exceptional_spirit
			}
			else_if = {
				limit = {
					has_country_flag = bim_lau_talented_spirit
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 2
					}
				}
				clr_country_flag = bim_lau_talented_spirit
			}
			else_if = {
				limit = {
					NOT = { has_country_flag = bim_lau_weak_spirit }
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 1
					}
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
			clr_country_flag = bim_lau_weak_spirit
		}
	}
	
	#Medium Ceremony
	option = {
		name = bim_lau_flavour.5.b
		custom_tooltip = bim_lau_spirit_update_tt
		ai_chance = {
			factor = 70
		}
		add_years_of_income = -1
		add_prestige = 5
		every_province = {
			limit = {
				owned_by = ROOT
				region = bomdan_region
			}
			add_province_modifier = {
				name = Y58_lot_dekkhang_ceremony
				duration = 1095
			}
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombOpulence
					value = 3
				}
			}
			if = {
				limit = {
					has_country_flag = bim_lau_mage_entombment
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 5
					}
				}
				clr_country_flag = bim_lau_mage_entombment
			}
			else_if = {
				limit = {
					has_country_flag = bim_lau_exceptional_spirit
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 3
					}
				}
				clr_country_flag = bim_lau_exceptional_spirit
			}
			else_if = {
				limit = {
					has_country_flag = bim_lau_talented_spirit
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 2
					}
				}
				clr_country_flag = bim_lau_talented_spirit
			}
			else_if = {
				limit = {
					NOT = { has_country_flag = bim_lau_weak_spirit }
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 1
					}
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
			clr_country_flag = bim_lau_weak_spirit
		}
	}
	
	#Only Family
	option = {
		name = bim_lau_flavour.5.c
		custom_tooltip = bim_lau_spirit_update_tt
		ai_chance = {
			factor = 15
		}
		add_years_of_income = -0.1
		every_province = {
			limit = {
				owned_by = ROOT
				region = bomdan_region
			}
			add_province_modifier = {
				name = Y58_lot_dekkhang_ceremony
				duration = 365
			}
		}
		
		hidden_effect = {
			if = {
				limit = {
					has_country_flag = bim_lau_mage_entombment
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 5
					}
				}
				clr_country_flag = bim_lau_mage_entombment
			}
			else_if = {
				limit = {
					has_country_flag = bim_lau_exceptional_spirit
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 3
					}
				}
				clr_country_flag = bim_lau_exceptional_spirit
			}
			else_if = {
				limit = {
					has_country_flag = bim_lau_talented_spirit
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 2
					}
				}
				clr_country_flag = bim_lau_talented_spirit
			}
			else_if = {
				limit = {
					NOT = { has_country_flag = bim_lau_weak_spirit }
				}
				4565 = {
					change_variable = {
						which = BimLauTombPower
						value = 1
					}
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
			clr_country_flag = bim_lau_weak_spirit
		}
	}
}

country_event = { #enemy general entombment
	id = bim_lau_flavour.6
	title = bim_lau_flavour.6.t
	desc = bim_lau_flavour.6.d
	picture = DEATH_OF_HEIR_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = bim_lau_flavour.6.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		ai_chance = {
			factor = 75
		}
		
		add_years_of_income = -0.3
		add_prestige = 5
		
		hidden_effect = {
			random_list = {
				33 = {
					4565 = {
						change_variable = {
							which = BimLauTombPower
							value = 5
						}
					}
				}
				67 = {
					4565 = {
						change_variable = {
							which = BimLauTombPower
							value = 1
						}
					}
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
		}
	}
	
	option = { #no
		name = bim_lau_flavour.6.b
	
		add_prestige = -5
		
		ai_chance = {
			factor = 25
		}
	}
}

country_event = { #entomb general
	id = bim_lau_flavour.7
	title = bim_lau_flavour.7.t
	desc = bim_lau_flavour.7.d
	picture = DEATH_OF_HEIR_eventPicture
	
	trigger = {
		owns_core_province = 4565
		controls = 4565
		OR = {
			tag = Y51
			capital_scope = { region = bomdan_region }
		}
		OR = {
			religion = high_philosophy
			religion = righteous_path
			religion = lefthand_path
			religion = mystic_accord
		}
	}
	
	mean_time_to_happen = {
		years = 20
	}
	
	option = { #entomb
		name = bim_lau_flavour.7.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_years_of_income = -0.5
		add_legitimacy = 5
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombPower
					value = 1
				}
				change_variable = {
					which = BimLauTombOpulence
					value = 1
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
		}
		
		ai_chance = {
			factor = 75
		}
	}
	
	option = { #no
		name = bim_lau_flavour.7.b
		
		add_army_tradition = -5
		
		ai_chance = {
			factor = 25
		}
	}
}

country_event = { #entomb advisor
	id = bim_lau_flavour.8
	title = bim_lau_flavour.8.t
	desc = bim_lau_flavour.8.d
	picture = DEATH_OF_HEIR_eventPicture
	
	trigger = {
		owns_core_province = 4565
		controls = 4565
		OR = {
			tag = Y51
			capital_scope = { region = bomdan_region }
		}
		OR = {
			religion = high_philosophy
			religion = righteous_path
			religion = lefthand_path
			religion = mystic_accord
		}
	}
	
	mean_time_to_happen = {
		years = 20
	}
	
	option = { #entomb
		name = bim_lau_flavour.8.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_years_of_income = -0.5
		add_adm_power = 15 
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombPower
					value = 1
				}
				change_variable = {
					which = BimLauTombOpulence
					value = 1
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
		}
		
		ai_chance = {
			factor = 75
		}
	}
	
	option = { #no
		name = bim_lau_flavour.8.b
		
		add_estate_loyalty = {
			estate = estate_church
			loyalty = -10
		}
		
		ai_chance = {
			factor = 25
		}
	}
}

country_event = { #entomb war mage
	id = bim_lau_flavour.9
	title = bim_lau_flavour.9.t
	desc = bim_lau_flavour.9.d
	picture = DEATH_OF_HEIR_eventPicture
	
	trigger = {
		owns_core_province = 4565
		controls = 4565
		OR = {
			tag = Y51
			capital_scope = { region = bomdan_region }
		}
		has_estate_privilege = estate_mages_battlemage_academies
		has_estate = estate_mages
		OR = {
			religion = high_philosophy
			religion = righteous_path
			religion = lefthand_path
			religion = mystic_accord
		}
	}
	
	mean_time_to_happen = {
		years = 35
	}
	
	option = { #entomb
		name = bim_lau_flavour.9.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_years_of_income = -0.7
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 5
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombPower
					value = 5
				}
				change_variable = {
					which = BimLauTombOpulence
					value = 2
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
		}
		
		ai_chance = {
			factor = 75
		}
	}
	
	option = { #no
		name = bim_lau_flavour.9.b
		
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		
		ai_chance = {
			factor = 25
		}
	}
}

country_event = { #entomb court mage
	id = bim_lau_flavour.10
	title = bim_lau_flavour.10.t
	desc = bim_lau_flavour.10.d
	picture = DEATH_OF_HEIR_eventPicture
	
	trigger = {
		owns_core_province = 4565
		controls = 4565
		OR = {
			tag = Y51
			capital_scope = { region = bomdan_region }
		}
		has_estate = estate_mages
		OR = {
			religion = high_philosophy
			religion = righteous_path
			religion = lefthand_path
			religion = mystic_accord
		}
	}
	
	mean_time_to_happen = {
		years = 35
	}
	
	option = { #entomb
		name = bim_lau_flavour.10.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_years_of_income = -0.7
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 5
		}
		
		hidden_effect = {
			4565 = {
				change_variable = {
					which = BimLauTombPower
					value = 5
				}
				change_variable = {
					which = BimLauTombOpulence
					value = 2
				}
			}
			if = {
				limit = {
					NOT = { has_country_flag = bim_lau_spirits_angry }
				}
				bim_lau_spirit_power = yes
			}
		}
		
		ai_chance = {
			factor = 75
		}
	}
	
	option = { #no
		name = bim_lau_flavour.10.b
		
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		
		ai_chance = {
			factor = 25
		}
	}
}

country_event = { #graverobbers
	id = bim_lau_flavour.11
	title = bim_lau_flavour.11.t
	desc = bim_lau_flavour.11.d
	picture = IMPORTANT_STATUE_eventPicture
	
	trigger = {
		owns_core_province = 4565
		controls = 4565
		4565 = {
			check_variable = {
				which = BimLauTombOpulence
				value = 30
			}
		}
	}
	
	mean_time_to_happen = {
		years = 50
	}
	
	option = { #hunt them
		name = bim_lau_flavour.11.a
		
		add_mil_power = -50
		
		ai_chance = {
			factor = 85
		}
	}
	
	option = { #let them leave
		name = bim_lau_flavour.11.b
		custom_tooltip = bim_lau_spirit_update_tt
		
		hidden_effect = {
			4565 = {
				subtract_variable = {
					which = BimLauTombOpulence
					value = 3
				}
			}
			bim_lau_spirit_power = yes
		}
		
		ai_chance = {
			factor = 15
		}
	}
}

#disaster events
country_event = { #spirits leave
	id = bim_lau_flavour.12
	title = bim_lau_flavour.12.t
	desc = bim_lau_flavour.12.d
	picture = ACCUSATION_eventPicture
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	trigger = {
		owns_core_province = 4565
		controls = 4565
		check_variable = {
			which = BimLauTombPower
			value = 5
		}
	}
	
	option = { #appease them
		name = bim_lau_flavour.12.a
		
		add_adm_power = -25
		add_dip_power = -25
		add_mil_power = -25
		
		ai_chance = {
			factor = 50
		}
	}
	
	option = { #let them leave
		name = bim_lau_flavour.12.b
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -10
		}
		
		hidden_effect = {
			4565 = {
				subtract_variable = {
					which = BimLauTombPower
					value = 5
				}
			}
		}
		
		ai_chance = {
			factor = 50
		}
	}
}

country_event = { #start disaster
	id = bim_lau_flavour.13
	title = bim_lau_flavour.13.t
	desc = bim_lau_flavour.13.d
	picture = PRAYING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	immediate = {
		hidden_effect = {
			set_country_flag = bim_lau_spirits_angry
		}
	}
	
	option = {
		name = bim_lau_flavour.13.a
	}
}

country_event = { #end disaster
	id = bim_lau_flavour.14
	title = bim_lau_flavour.14.t
	desc = bim_lau_flavour.14.d
	picture = RELIGION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	immediate = {
		hidden_effect = {
			clr_country_flag = bim_lau_spirits_angry
		}
	}
	
	option = {
		name = bim_lau_flavour.14.a
	}
}

country_event = { #spirits cause earthquake
	id = bim_lau_flavour.15
	title = bim_lau_flavour.15.t
	desc = bim_lau_flavour.15.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = bim_lau_flavour.15.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		4565 = {
			add_devastation = 20
		}
		
		hidden_effect = {
			4565 = {
				subtract_variable = {
					which = BimLauTombPower
					value = 5
				}
				subtract_variable = {
					which = BimLauTombOpulence
					value = 5
				}
			}
		}
	}
}

country_event = { #fortifications damaged
	id = bim_lau_flavour.16
	title = bim_lau_flavour.16.t
	desc = bim_lau_flavour.16.d
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = bim_lau_flavour.16.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_mil_power = -50
		
		hidden_effect = {
			4565 = {
				subtract_variable = {
					which = BimLauTombPower
					value = 5
				}
			}
		}
	}
}

country_event = { #spirits scare residents
	id = bim_lau_flavour.17
	title = bim_lau_flavour.17.t
	desc = bim_lau_flavour.17.d
	picture = WOUNDED_SOLDIERS_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = bim_lau_flavour.17.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_adm_power = -50
		
		hidden_effect = {
			4565 = {
				subtract_variable = {
					which = BimLauTombPower
					value = 5
				}
			}
		}
	}
}

country_event = { #fresko destroyed
	id = bim_lau_flavour.18
	title = bim_lau_flavour.18.t
	desc = bim_lau_flavour.18.d
	picture = RELIGION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = bim_lau_flavour.18.a
		custom_tooltip = bim_lau_spirit_update_tt
		
		add_dip_power = -50
		
		hidden_effect = {
			4565 = {
				subtract_variable = {
					which = BimLauTombOpulence
					value = 5
				}
				subtract_variable = {
					which = BimLauTombPower
					value = 2
				}
			}
		}
	}
}

country_event = {
	id = bim_lau_flavour.20
	title = bim_lau_flavour.20.t
	desc = bim_lau_flavour.20.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	option = {
		name = bim_lau_flavour.20.a
		
		ai_chance = {
			factor = 85
		}
		
		4409 = {
			move_capital_effect = yes
			change_culture = royal_harimari
			change_religion = ROOT
			add_base_tax = 2
			add_base_production = 2
			add_base_manpower = 2
		}
		
		add_country_modifier = {
			name = "centralised_state"
			duration = 9125
		}
	}
	
	option = {
		name = bim_lau_flavour.20.b
		
		ai_chance = {
			factor = 15
		}
		
		4565 = {
			add_base_tax = 2
			add_base_production = 2
			add_base_manpower = 2
		}
	}
}

country_event = {
	id = bim_lau_flavour.21
	title = bim_lau_flavour.21.t
	desc = bim_lau_flavour.21.d
	picture = GREAT_BUILDING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		owns_core_province = 4565
	}
	
	option = {
		name = bim_lau_flavour.21.a
		
		4565 = {
			remove_province_modifier = Y51_necropolis_repairs
			add_permanent_province_modifier = {
				name = Y51_necropolis_1
				duration = -1
			}
		}
	}
}