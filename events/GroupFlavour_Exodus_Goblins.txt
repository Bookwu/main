namespace = exodus_goblin_flavour

#OSC harass elves in Greysheep
country_event = {
	id = exodus_goblin_flavour.1
	title = exodus_goblin_flavour.1.t
	desc = exodus_goblin_flavour.1.d
	picture = { picture =  COURT_eventPicture }
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = U13
	}
	
	option = { #Let them be
		name = "exodus_goblin_flavour.1.a"
		651 = { change_religion = old_bulwari_sun_cult }
		654 = { change_religion = old_bulwari_sun_cult }
	}
	
	option = { #Make them stop
		name = "exodus_goblin_flavour.1.b"
		small_increase_of_elven_tolerance_effect = yes
		657 = { add_unrest = 5 }
		658 = { add_unrest = 5 }
	}
	
}

namespace = flavour_overclan

#Choosing the name
country_event = {
	id = flavour_overclan.1
	title = flavour_overclan.1.t
	desc = flavour_overclan.1.d
	picture = { picture =  COURT_eventPicture }
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = F78
	}
	
	option = { #The Overclan
		name = "flavour_overclan.1.a"
		add_prestige = 5
	}
	
	option = { #Bahari Overclan
		name = "flavour_overclan.1.b"
		override_country_name = BAHARI_OVERCLAN
	}
	
	option = { #Aqatbar Overclan
		trigger = { capital = 536 }
		name = "flavour_overclan.1.c"
		override_country_name = AQATBAR_OVERCLAN
	}
	
}