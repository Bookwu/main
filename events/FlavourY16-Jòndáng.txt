
namespace = jondang_events

country_event = { #Start with low republican tradition
	id = jondang_events.1
	title = jondang_events.1.t
	desc = jondang_events.1.d
	picture = CORRUPTION_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = Y16
	}
	option = {
		name = jondang_events.1.a
		
		add_republican_tradition = -60
	}	
}